abstract class AppConfig {
  //App name
  String appName() => "Test";

  //App version
  String appVersion() => "1.0.0";
}
